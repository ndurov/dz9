if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}
if (!Array.prototype.findIndex) {
  Array.prototype.findIndex = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return i;
      }
    }
    return -1;
  };
}

function Student(student, point) {
	this.student = student;
	this.point   = point;
}
Student.prototype.show = function() {
	console.log('Студент %s набрал %d баллов', this.student, this.point);
};

function StudentList(group, list) {
	var students = [],
	    list     = list !== undefined ? list : [];
	for (i = 0, imax = list.length / 2; i < imax; i++) {
		this.add(list[i * 2], list[i * 2 + 1]);
	}
	this.group = group;
}
StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function(student, point) {
	var objStudent = student.hasOwnProperty('student') ? student : new Student(student, point);
	this.push(objStudent);
};

var hj2List = [
		'Алексей Петров', 0,
		'Ирина Овчинникова', 60,
		'Глеб Стукалов', 30,
		'Антон Павлович', 30,
		'Виктория Заровская', 30,
		'Алексей Левенец', 70,
		'Тимур Вамуш', 30,
		'Евгений Прочан', 60,
		'Александр Малов', 0
	],
	hj2 = new StudentList('HJ-2', hj2List);

hj2.add('Николай Фролов', 40);
hj2.add('Олег Боровой', 20);

var html7 = new StudentList('HTML-7');

html7.add('Мария Денисова', 60);
html7.add('Андрей Грачев', 100);
html7.add('Софья Прокофьева', 80);
html7.add('Мария Михайлец', 50);
html7.add('Сергей Смородников', 150);
html7.add('Андрей Лобов', 90);
html7.add('Алексей Копылов', 80);

StudentList.prototype.show = function() {
	console.log('\nГруппа %s (%d студентов):', this.group, this.length);
	this.forEach(function (obj, i) {
		obj.show();
	});
};
hj2.show();
html7.show();

StudentList.prototype.move = function(student, group) {
	var objIndex = this.findIndex(function (obj) {
	    	return obj.student == student;
	    });
	if (objIndex != -1) {
		group.add(this[objIndex]);
		this.splice(objIndex, 1);
	} else {
		console.log('\nСтудента %s нет в группе %s', student, this.group);
	}
};
hj2.move('Алексей Левенец', html7);

Student.prototype.valueOf = function() {
	return this.point;
};
StudentList.prototype.max = function() {
	var maxPoint = Math.max.apply(null, this);
	return this.find(function (obj) {
		return obj.point == maxPoint;
	});
};

console.log('\nЛучший студент группы %s:', hj2.group);
hj2.max().show();